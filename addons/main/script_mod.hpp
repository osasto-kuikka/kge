#define MAINPREFIX z
#define PREFIX kge

#define MAJOR 1
#define MINOR 0
#define PATCHLVL 6
#define BUILD 0

#define VERSION MAJOR.MINOR.PATCHLVL.BUILD
#define VERSION_AR MAJOR,MINOR,PATCHLVL,BUILD

// MINIMAL required version for the Mod. Components can specify others..
#define REQUIRED_VERSION 0.5
